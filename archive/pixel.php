<?php

$file = fopen("pixels.txt", 'r') or die("error: can't read from file");
$pixelStr = fgets($file);
fclose($file);

$change = $_POST["change"];

if (isset($change)) {
	$pixels = explode('|', $pixelStr);

	$change = intval($change);
	$changeColor = $change % 8;
	$changePixel = intdiv($change, 8);
	if ($changePixel >= count($pixels)) die("error: request out of bounds");

	$currColor = $pixels[$changePixel] % 8;
	$currLocks = intdiv($pixels[$changePixel], 8);
	if ($currColor === $changeColor) {
		$currLocks = min($currLocks + 1, 1000);
	} else if ($currLocks === 0) {
		$currColor = $changeColor;
	} else {
		--$currLocks;
	}
	$pixels[$changePixel] = 8 * $currLocks + $currColor;

	$file = fopen("pixels.txt", 'w') or die("error: can't write to file");
	$pixelStr = implode('|', $pixels);
	fwrite($file, $pixelStr);
	fclose($file);
}

echo $pixelStr;

?>
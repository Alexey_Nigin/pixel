# The PixelLock Archive

I initially created PixelLock to learn PHP, back when I thought I was gonna work
on automated PHP security assessment tools in my research job. My research ended
up being on a completely different topic, but I made good memories drawing stuff
in PixelLock with my friends and grandparents.

Now, in May 2023, as I am about to migrate `nigin.rocks` to a new server, I want
to preserve PixelLock, for nostalgia sake if nothing else. But I really don't
want to use PHP on my new server. So, I am writing a new Python backend for
PixelLock, which will live in the `nigin-rocks` repo. And I am gonna spruce up
the frontend a bit too, while keeping as close as I can to the original feel.

As a result, the old PHP backend and some other files in this repo are gonna
become irrelevant, but for posterity I will save them in this archive directory.
Nothing in this directory is going to be used in any way on the new server.

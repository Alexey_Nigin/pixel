// Define globals

const BASE_64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "-_";
const SIZE = 64;
const ZOOM = 10;
const COLORS = [
	"#000", "#00f", "#0f0", "#0ff", "#f00", "#f0f", "#ff0", "#fff"
];

let $display = $("#display");
$display.attr("width", SIZE * ZOOM);
$display.attr("height", SIZE * ZOOM);
let display = $display[0].getContext("2d");
let $apply = $("#apply");

// Selected color in the pane on the right
let selectedColor = 0;
// Selected pixel on the canvas
let selectedPixel = {x: 0, y: 0};
// 2D (SIZE x SIZE) array of objects in format {color: <int>, numLocks: <int>}
let canvas = [];
for (let x = 0; x < SIZE; ++x) {
	canvas.push([]);
	for (let y = 0; y < SIZE; ++y) {
		canvas[x].push({color: 0, numLocks: 0});
	}
}



// Update the screen

function update() {
	for (let x = 0; x < SIZE; ++x) {
		for (let y = 0; y < SIZE; ++y) {
			display.fillStyle = COLORS[canvas[x][y].color];
			display.fillRect(x * ZOOM, y * ZOOM, ZOOM, ZOOM);
		}
	}

	let x = selectedPixel.x;
	let y = Math.floor(selectedPixel.y);
	display.strokeStyle = "#666";
	display.lineWidth = 3;
	display.strokeRect(x * ZOOM, y * ZOOM, ZOOM, ZOOM);

	let currColor = canvas[x][y].color;
	let currLocks = canvas[x][y].numLocks;
	$("#current").css("background-color", COLORS[currColor]);
	$("#num-locks").text(currLocks);
	$("#lock").css("display", (currLocks === 0) ? "none" : "block");
};



// Register input

for (let c = 0; c < COLORS.length; ++c) {
	$("#c" + c).on("click", () => {
		$("#c" + selectedColor).css("border-width", 1);
		selectedColor = c;
		$("#c" + selectedColor).css("border-width", 3);
		$("#apply").css("background-color", COLORS[selectedColor]);
	});
}


$display.on("click", function(event) {
	let x = Math.floor(event.offsetX / ZOOM);
	let y = Math.floor(event.offsetY / ZOOM);
	selectedPixel.x = x;
	selectedPixel.y = y;
	update();
});



// Talk to the server

async function receive(response) {
	let canvasZip = await response.json();

	let pointer = 0;

	function readChar() {
		console.assert(pointer < canvasZip.length);
		char = canvasZip[pointer];
		++pointer;
		console.assert(BASE_64_ALPHABET.indexOf(char) > -1);
		return BASE_64_ALPHABET.indexOf(char);
	}

	for (let y = 0; y < SIZE; ++y) {
		for (let x = 0; x < SIZE; ++x) {
			let char0 = readChar();
			let singleNumber;
			if(char0 < 32) {
				singleNumber = char0;
			} else {
				let char1 = readChar();
				singleNumber = 64 * (char0 - 32) + char1;
			}
			canvas[x][y].color = singleNumber % 8;
			canvas[x][y].numLocks = Math.floor(singleNumber / 8);
		}
	}

	console.assert(pointer === canvasZip.length);
	update();
}


function countdown(time) {
	if (time === 0) {
		$apply.text("Apply");
		$apply.attr("disabled", false);
	} else {
		$apply.text(time);
		setTimeout(countdown.bind(null, time - 1), 1000);		
	}
};


$apply.on("click", () => {
	fetch(
		"canvas/",
		{
			method: "PATCH",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify({
				x: selectedPixel.x,
				y: selectedPixel.y,
				color: selectedColor
			})
		}
	)
		.then(response => receive(response))
		.catch(error => console.log(`PATCH error: ${JSON.stringify(error)}`));
	$apply.attr("disabled", true);
	countdown(5);
});


function heartbeat() {
	fetch("canvas/")
		.then(response => receive(response))
		.then(() => setTimeout(heartbeat, 1000))
		.catch(error => {
			console.log(`GET error: ${JSON.stringify(error)}`);
			setTimeout(heartbeat, 5000);
		});
}

heartbeat();
